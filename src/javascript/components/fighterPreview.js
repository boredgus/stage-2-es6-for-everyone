import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImg = createFighterImage(fighter);
    const fighterName = createElement({
      tagName: 'h3',
      className: 'fighter-preview___name'
    });
    fighterName.innerHTML = fighter.name;
    const fighterInfo = createFighterInfo(fighter);
    fighterElement.append(fighterImg, fighterName, fighterInfo);
  }

  return fighterElement;
}

function createFighterInfo(fighter) {
  const { health, attack, defense } = fighter;
  const fighterProperties = { health, attack, defense };
  const fighterInfoElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info-container'
  });
  Object.keys(fighterProperties).map((prop) => {
    const fighterInfoRow = createElement({ tagName: 'div', className: 'fighter-preview___info-row' });

    let propName = createElement({
      tagName: 'td',
      className: 'bold',
    });
    let propValue = createElement({ tagName: 'td' });

    propName.innerHTML = prop[0].toUpperCase() + prop.slice(1);
    propValue.innerHTML = fighterProperties[prop];

    fighterInfoRow.append(propName, propValue);
    fighterInfoElement.append(fighterInfoRow);
  });

  return fighterInfoElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
