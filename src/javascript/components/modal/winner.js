import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

const playAgain = () => open(location, '_self');

export function showWinnerModal(fighter) {
  const title = `Congrats! ${fighter.name} won!`;
  const bodyElement = createWinnerModalWindowBody(fighter);

  showModal({
    title,
    bodyElement,
    onClose: () => playAgain(),
  });
}

function createWinnerModalWindowBody(fighter) {
  const element = createElement({ tagName: 'div', className: 'modal-body' });
  const winnerImage = createFighterImage(fighter);
  const playAgainBlock = createPlayAgainBlock();

  element.append(winnerImage, playAgainBlock);

  return element;
}

function createPlayAgainBlock() {
  const playAgainContainer = createElement({ tagName: 'div', className: 'play-again___container' });
  const playAgainQuestion = createElement({ tagName: 'div', className: 'play-again___question' });
  const answersContainer = createElement({ tagName: 'div', className: 'play-again___answers-container' });
  const answer1 = createElement({ tagName: 'button', className: 'play-again___answer-btn' });
  const answer2 = createElement({ tagName: 'button', className: 'play-again___answer-btn' });

  playAgainQuestion.innerHTML = 'Wanna play again?';
  answer1.innerHTML = 'Yes!';
  answer2.innerHTML = 'Of course!'

  answer1.onclick = () => playAgain();
  answer2.onclick = () => playAgain();

  answersContainer.append(answer1, answer2);
  playAgainContainer.append(playAgainQuestion, answersContainer);

  return playAgainContainer;
}