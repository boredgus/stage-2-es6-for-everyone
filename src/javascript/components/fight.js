import { Game } from './game';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const game = new Game(firstFighter, secondFighter);

    let interval = setInterval(() => {
      if (!(game.firstPlayer.currentHealth > 0 && game.secondPlayer.currentHealth > 0)) {
        clearInterval(interval);
        const winner = game.firstPlayer.currentHealth <= 0 ? secondFighter : firstFighter;
        game.firstPlayer.currentHealth <= 0
          ? game.firstPlayer.lose()
          : game.secondPlayer.lose();
          game.unbindKeywordEvents();
        resolve(winner);
      }
    }, 10);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage <= 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
