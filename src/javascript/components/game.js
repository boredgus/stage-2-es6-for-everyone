import Player from './player';
import { controls } from '../../constants/controls';
import { sides } from '../../constants/sides';
import { getDamage } from './fight';

export class Game {
	constructor(firstFighter, secondFighter) {
		this.firstPlayer = new Player(firstFighter, sides.firstPlayer);
		this.secondPlayer = new Player(secondFighter, sides.secondPlayer);
		this.pressedButtons = new Set();

		this.bindKeywordEvents();
	}

	bindKeywordEvents() {
		document.onkeydown = (event) => this.pressButton(event);
		document.onkeyup = (event) => this.releaseButton(event);
	}

	unbindKeywordEvents() {
		document.onkeydown = () => {};
		document.onkeyup = () => {};
	}

	pressButton({ code }) {
		if (!this.pressedButtons.has(code)) {
			this.pressedButtons.add(code);
			let canAttack = !(this.pressedButtons.has(controls.PlayerTwoBlock) || this.pressedButtons.has(controls.PlayerOneBlock));
			
			switch (code) {
				case controls.PlayerOneAttack: {
					if (canAttack) {
						this.secondPlayer.currentHealth -= getDamage(this.firstPlayer.fighter, this.secondPlayer.fighter);
						this.firstPlayer.attackMove();
					}
					break;
				}

				case controls.PlayerTwoAttack: {
					if (canAttack) {
						this.firstPlayer.currentHealth -= getDamage(this.secondPlayer.fighter, this.firstPlayer.fighter);
						this.secondPlayer.attackMove();
					}
					break;
				}

				case controls.PlayerOneBlock: {
					this.firstPlayer.startBlockMove();
					break;
				}

				case controls.PlayerTwoBlock: {
					this.secondPlayer.startBlockMove();
					break;
				}

				case controls.PlayerOneCriticalHitCombination[0]
					|| controls.PlayerOneCriticalHitCombination[1]
					|| controls.PlayerOneCriticalHitCombination[2]: {
						let isAllKeysPressed = controls.PlayerOneCriticalHitCombination.every((key) => this.pressedButtons.has(key));
						let canHit = isAllKeysPressed
							&& !this.pressedButtons.has(controls.PlayerOneBlock)
							&& this.firstPlayer.canHitCriticaly;
						if (canHit) {
							this.secondPlayer.currentHealth -= 2 * this.firstPlayer.fighter.attack;
							this.firstPlayer.canHitCriticaly = false;
							this.firstPlayer.criticalHitMove();
							setTimeout(() => this.firstPlayer.canHitCriticaly = true, 10000);
						}
						break;
					}

				case controls.PlayerTwoCriticalHitCombination[0]
					|| controls.PlayerTwoCriticalHitCombination[1]
					|| controls.PlayerTwoCriticalHitCombination[2]: {
						let isAllKeysPressed = controls.PlayerTwoCriticalHitCombination.every((key) => this.pressedButtons.has(key));
						let canHit = isAllKeysPressed
							&& !this.pressedButtons.has(controls.PlayerTwoBlock)
							&& this.secondPlayer.canHitCriticaly;
						if (canHit) {
							this.firstPlayer.currentHealth -= 2 * this.secondPlayer.fighter.attack;
							this.secondPlayer.canHitCriticaly = false;
							this.secondPlayer.criticalHitMove();
							setTimeout(() => this.secondPlayer.canHitCriticaly = true, 10000);
						}
						break;
					}

				default: break;
			}

			this.updateHealthBars();
		}
	}

	releaseButton({ code }) {
		if (this.pressedButtons.has(code)) {
			this.pressedButtons.delete(code);
			if (code === controls.PlayerOneBlock) {
				this.firstPlayer.endMove();
			}
			else if (code === controls.PlayerTwoBlock) {
				this.secondPlayer.endMove();
			}
		}
	}

	updateHealthBars() {
		const leftHealthBar = document.querySelector('#left-fighter-indicator');
		const rightHealthBar = document.querySelector('#right-fighter-indicator');

		leftHealthBar.style.width = this.firstPlayer.currentHealth <= 0
			? '0%'
			: (this.firstPlayer.currentHealth / this.firstPlayer.fighter.health * 100) + '%';
		rightHealthBar.style.width = this.secondPlayer.currentHealth <= 0
			? '0%'
			: (this.secondPlayer.currentHealth / this.secondPlayer.fighter.health * 100) + '%';
	}
}
