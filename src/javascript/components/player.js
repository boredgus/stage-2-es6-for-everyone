import { moveSteps } from '../../constants/moveSteps';

export default class Player {
	constructor(fighter, side) {
		this.fighter = fighter;
		this.currentHealth = fighter.health;
		this.canHitCriticaly = true;
		this.character = this.getLocation(`.arena___${side}-fighter`);
		this.side = side;
	}

	getLocation(selector) {
		return document.querySelector(selector);
	}

	startBlockMove() {
		this.character.style[this.side] = moveSteps.blockMove;
	}

	endMove() {
		this.character.style[this.side] = moveSteps.initialPosition;
	}

	attackMove() {
		this.character.style[this.side] = moveSteps.attackMove;
		setTimeout(() => this.endMove(), 150);
	}

	criticalHitMove() {
		this.character.style[this.side] = moveSteps.criticalHitMove;
		setTimeout(() => this.endMove(), 300);
	}

	lose() {
		this.character.classList.add('loser');
	}
}
