export const moveSteps = {
    initialPosition: '0px',
    attackMove: '100px',
    blockMove: '-100px',
    criticalHitMove: '150px',
}