# stage-2-es6-for-everyone

## Intro
2nd homework from 2nd stage of [Binary Studio Academy](https://academy.binary-studio.com/ua/).

## Description
Street Fighter is a simple browser game like a Mortal Combat.

## Controls
1. First player
 - `A` - attack
 - `D` - defend
 - `Q + W + E` - critical attack
1. Second player
 - `J` - attack
 - `L` - defend
 - `U + I + O` - critical attack

> **_NOTE:_**
> - You can use critical attack once per 10 seconds only.
> - You cannot attack while you are defending

## Instalation

`npm install`

`npm run build`

`npm run start`

open http://localhost:8080/
